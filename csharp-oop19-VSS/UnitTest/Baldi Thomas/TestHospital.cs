﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using csharp_oop19_VSS.model.people;
using csharp_oop19_VSS.model.places;
using System.Collections.Generic;
using System;
using csharp_oop19_VSS.model.virus;

namespace UnitTest.places
{
    [TestClass]
    public class TestHospital
    {
        IHospital hospital = new HospitalImpl();
        IVirus virus = new VirusFactoryImpl(1, 2, 8, 10, 2, 10).CreateVirus();
        /// <summary>
        /// Tests the entry method.
        /// </summary>
        [TestMethod]
        public void TestEntry()
        {
            int nPeople = 20;
            List<IPerson> people = new List<IPerson>();
            for(int i= 0; i < nPeople; i++)
            {
                people.Add(new PersonImpl());
            }
            IPerson person = new PersonImpl();

            people.ForEach(p => p.Infect(virus));
            people.ForEach(p => this.hospital.Enter(p, 1));
            Assert.IsTrue(this.hospital.IsAnyoneInHospital());
            try
            {
                this.hospital.Enter(person, 2);
                Assert.Fail("a person can't enter to the hospital if isn't ill");
            }
            catch(ArgumentException ex) { }

            person.Infect(virus);
            this.hospital.Enter(person, 2);
            try
            {
                this.hospital.Enter(person, 3);
                Assert.Fail("the person who want enter is already inside");
            }
            catch (InvalidOperationException ex) { }


        }
        /// <summary>
        /// Tests the exitWithOutcome method.
        /// </summary>
        [TestMethod]
        public void TestExitWithOutcome()
        {
            int nPeople = 20;
            List<IPerson> people = new List<IPerson>();
            for (int i = 0; i < nPeople; i++)
            {
                people.Add(new PersonImpl());
            }
            people.ForEach(p => p.Infect(virus));
            people.ForEach(p => this.hospital.Enter(p, 1));
            this.hospital.ExitWithOutcome(1).GetRecoveredPeople.ForEach(p => Assert.IsTrue(Status.SUSCEPTIBLE == p.Status));

        }
        /// <summary>
        /// Tests the isAnyoneInHospital method.
        /// </summary>
        [TestMethod]
        public void TestIsAnyonrInHospital()
        {
            int nPeople = 20;
            List<IPerson> people = new List<IPerson>();
            for (int i = 0; i < nPeople; i++)
            {
                people.Add(new PersonImpl());
            }
            people.ForEach(p => p.Infect(virus));
            Assert.IsFalse(this.hospital.IsAnyoneInHospital());
            people.ForEach(p => this.hospital.Enter(p, 1));
            Assert.IsTrue(this.hospital.IsAnyoneInHospital());

        }
    }
}
