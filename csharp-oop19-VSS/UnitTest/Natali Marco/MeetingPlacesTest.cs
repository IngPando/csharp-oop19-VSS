﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using csharp_oop19_VSS.model.places;
using csharp_oop19_VSS.model.people;
using System.Collections.Generic;

namespace UnitTest.places
{
    /// <summary>
    /// Meeting places testing class.
    /// </summary>
    [TestClass]
    public class MeetingPlacesTest
    {
        private readonly IMeetingPlace meetingPlace = new MeetingPlaceImpl();

        private List<IPerson> GenerateList(int nPeople)
        {
            List<IPerson> people = new List<IPerson>();
            for(int i=0; i<nPeople; i++)
            {
                IPerson person = new PersonImpl();
                people.Add(person);
            }
            return people;
        }

        /// <summary>
        /// Tests the entry method.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void TestEntry()
        {
            List<IPerson> people = GenerateList(20);
            people.ForEach(p => meetingPlace.Enter(p, 1));
            people.ForEach(p => Assert.IsTrue(meetingPlace.CheckPresence(p)));
            IPerson person = new PersonImpl();
            meetingPlace.Enter(person, 2);
            Assert.IsTrue(meetingPlace.CheckPresence(person));
            meetingPlace.Enter(person, 4);
            Assert.Fail("The person who enters is already inside");
        }

        /// <summary>
        /// Tests the exit method.
        /// </summary>
        [TestMethod]
        public void TestExit()
        {
            List<IPerson> people = GenerateList(20);
            people.ForEach(p => meetingPlace.Enter(p, 1));
            meetingPlace.Exit(1);
            people.ForEach(p => Assert.IsFalse(meetingPlace.CheckPresence(p)));
            IPerson person = new PersonImpl();
            meetingPlace.Enter(person, 2);
            meetingPlace.Exit(2);
            Assert.IsFalse(meetingPlace.CheckPresence(person));
            Assert.IsTrue(meetingPlace.Exit(2).Count==0);
        }

        /// <summary>
        /// Tests the ExitSinglePerson method.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void TestExitSinglePerson()
        {
            List<IPerson> people = GenerateList(20);
            IPerson person = new PersonImpl();
            people.ForEach(p => meetingPlace.Enter(p, 1));
            people.ForEach(p => meetingPlace.ExitSinglePerson(p));
            people.ForEach(p => Assert.IsFalse(meetingPlace.CheckPresence(p)));
            meetingPlace.Enter(person, 1);
            meetingPlace.ExitSinglePerson(person);
            Assert.IsFalse(meetingPlace.CheckPresence(person));
            meetingPlace.ExitSinglePerson(person);
            Assert.Fail("The place doesn't contain the person");
        }

        /// <summary>
        /// Tests the checkPresence method.
        /// </summary>
        [TestMethod]
        public void TestCheckPresence()
        {
            IPerson person = new PersonImpl();
            meetingPlace.Enter(person, 1);
            Assert.IsTrue(meetingPlace.CheckPresence(person));
            meetingPlace.Exit(1);
            Assert.IsFalse(meetingPlace.CheckPresence(person));
        }

        /// <summary>
        /// Tests the isOpen method.
        /// </summary>
        [TestMethod]
        public void TestIsOpen()
        {
            Assert.IsTrue(meetingPlace.IsOpen);
            meetingPlace.Close();
            Assert.IsFalse(meetingPlace.IsOpen);
            meetingPlace.Open();
            Assert.IsTrue(meetingPlace.IsOpen);
        }

        /// <summary>
        /// Tests the isFull method.
        /// </summary>
        [TestMethod]
        public void TestIsFull()
        {
            IPerson person = new PersonImpl();
            List<IPerson> people = GenerateList(19);
            people.ForEach(p => meetingPlace.Enter(p, 1));
            meetingPlace.Enter(person, 2);
            Assert.IsTrue(meetingPlace.IsFull());
            meetingPlace.Exit(1);
            Assert.IsFalse(meetingPlace.IsFull());
        }
    }
}