﻿using System;
using csharp_oop19_VSS.model.people;

namespace csharp_oop19_VSS.model.places
{
    public class HomeImpl : AbstractPlace,IHome
    {
        private const int MAX_TIME = 48;
        private readonly double homeTendency;
        private double homeTendencyIncrement;

        /// <summary>
        /// Constructor method for the home with default value.
        /// </summary>
        /// <param name="homeTendency">The percentage of time that a people spend at home.</param>
        public HomeImpl(double homeTendency)
        {
            this.homeTendency = homeTendency;
            HomeTendencyIncrement = 0;
        }

        public double HomeTendencyIncrement
        {
            set
            {
                homeTendencyIncrement = value;
            }
        }

        public bool CheckPresence(IPerson person)
        {
            return GetAllPeople().Contains(person);
        }

        private int CalculateTime()
        {
            Random r = new Random();
            return r.Next((int)Math.Round(1 + 2 * MAX_TIME * (homeTendency + homeTendencyIncrement) / 100));
        }

        public override void Enter(IPerson person, int time)
        {
            int exitTime = time + CalculateTime();
            base.Enter(person, exitTime);
        } 


    }
}
