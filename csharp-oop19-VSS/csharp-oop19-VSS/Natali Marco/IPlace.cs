﻿using csharp_oop19_VSS.model.people;
using System.Collections.Generic;

namespace csharp_oop19_VSS.model.places
{
    public interface IPlace
    {
        /// <summary>
        ///Inserts a person into the place. 
        /// </summary>
        /// <param name="person">person the person who enters the place.</param>
        /// <param name="time">the instant the person has to leave</param>
        void Enter(IPerson person, int time);

        /// <summary>
        /// Removes people from the place.
        /// </summary>
        /// <param name="time">the actual instant</param>
        /// <returns>list of people leaving the place at a given instant</returns>
        List<IPerson> Exit(int time);

        /// <summary>
        ///Removes a person from the place.
        /// </summary>
        /// <param name="person">the person to be removed.</param>
        void ExitSinglePerson(IPerson person);

        /// <summary>
        /// Method that gets all the people inside the place.
        /// </summary>
        /// <returns>list of all people inside the place.</returns>
        List<IPerson> GetAllPeople();
    }
}
