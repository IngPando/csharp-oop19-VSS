﻿using csharp_oop19_VSS.model.people;
using System.Collections.Generic;

namespace csharp_oop19_VSS.model.places
{
    public interface IMeetingPlace:IPlace
    {
        /// <summary>
        /// Checks if the place is open.
        /// </summary>
        bool IsOpen { get; set; }

        /// <summary>
        /// Checks if the number of people in the place has reached maximum capacity.
        /// </summary>
        /// <returns>true if the number of people equals max capacity</returns>
        /// <returns>false if the number of people is less than max capacity.</returns>
        bool IsFull();

        /// <summary>
        /// When a person enters the place check if there are new infections.
        /// </summary>
        /// <param name="person">the person who enters the place.</param>
        /// <param name="time"> the actual instant</param>
        /// <returns>list of new-infected people.</returns>
        List<IPerson> EnterAndInfect(IPerson person, int time);

        /// <summary>
        /// Reopens the place after alert state has ended.
        /// </summary>
        void Open();

        /// <summary>
        /// Close the place, nobody can enter.
        /// </summary>
        /// <returns>list of people inside the place.</returns>
        List<IPerson> Close();

        /// <summary>
        /// Check if a person is in the place.
        /// </summary>
        /// <param name="person">person the person to look for in the place.</param>
        /// <returns>true if the person is present. </returns>
        /// <returns>false if the person isn't present. </returns>
        bool CheckPresence(IPerson person);
    }
}
