﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using csharp_oop19_VSS.model.people;

namespace csharp_oop19_VSS.controller
{
    public class PeopleManager : IPeopleManager
    {
        private readonly double birthRate;
        private readonly double deathRate;
        private readonly double stayHomeRate;
        private double homeTendencyIncrement;
        private readonly Random rand = new Random();

        public PeopleManager(double birthRate, double deathRate, double stayHomeRate)
        {
            this.birthRate = birthRate;
            this.deathRate = deathRate;
            this.stayHomeRate = stayHomeRate;
            this.homeTendencyIncrement = 0;
        }

        public List<IPerson> Birth(int totPerson)
        {
            int peopleToGenerate = rand.Next((int)(totPerson * birthRate / 100) + 1);
            return GeneratePerson(peopleToGenerate);
        }

        public List<IPerson> Death(List<IPerson> list)
        {
            list=list.Where(p => p.Status != Status.INFECTED).ToList();
            int peopleWhoDie = rand.Next((int)(list.Count() * deathRate / 100) + 1);
            return SelectRandomPeople(peopleWhoDie, list);
        }

        public List<IPerson> GoHome(List<IPerson> list)
        {
            int peopleWhoGoHome = rand.Next((int)(list.Count() * (stayHomeRate + homeTendencyIncrement) / 100) + 1);
            return SelectRandomPeople(peopleWhoGoHome, list);
        }

        /// <summary>
        /// generates new person 
        /// </summary>
        /// <param name="number">the number of person to generate</param>
        /// <returns>a list of new person</returns>
        private List<IPerson> GeneratePerson(int number)
        {
            List<IPerson> list = new List<IPerson>();
            for (int i = 0; i < number; i++)
            {
                list.Add(new PersonImpl());
            }
            return list;
        }

        /// <summary>
        /// select random people from a list
        /// </summary>
        /// <param name="num">number of people to select</param>
        /// <param name="list">list op people</param>
        /// <returns>a new list with random chosen people</returns>
        private List<IPerson> SelectRandomPeople(int num, List<IPerson> list)
        {
            List<IPerson> randomPeople = new List<IPerson>();
            for(int i=0;i<num;i++)
            {
                randomPeople.Add(list.ElementAt(rand.Next(list.Count)));
            }
            return randomPeople;
        }

        public double SetHomeTendencyIncrement
        {
            set
            {
                this.homeTendencyIncrement = value;
            }
        }
    }
}
