﻿using csharp_oop19_VSS.model.places;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp_oop19_VSS.model.places
{
    /// <summary>
    /// Interface that models the place where ill people are hospitalized.
    /// </summary>
    public interface IHospital : IPlace
    {
        /// <summary>
        /// Removes people from the hospital giving them the outcome.
        /// </summary>
        /// <param name="time">time the actual instant</param>
        /// <returns>Outcome of people hospitalization</returns>
        IHospitalizationOutcome ExitWithOutcome(int time);

        /// <summary>
        /// Checks if there are people in the hospital.
        /// </summary>
        /// <returns>True if one or more people are in the hospital 
        ///        False if nobody is in the hospital</returns>
        bool IsAnyoneInHospital();
    }
}
