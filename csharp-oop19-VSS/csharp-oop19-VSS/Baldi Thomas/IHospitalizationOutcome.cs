﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using csharp_oop19_VSS.model.people;

namespace csharp_oop19_VSS.model.places
{
    /// <summary>
    /// Interface that models the outcome of the hospitalization of a group of ill people.
    /// </summary>
    public interface IHospitalizationOutcome
    {
        /// <summary>
        /// Gets the people dead from the virus.
        /// </summary>
        /// <returns>List of dead people</returns>
        List<IPerson> GetDeadPeople { get; }

        /// <summary>
        /// Gets the people healed from the virus.
        /// </summary>
        /// <returns>List of recovered people</returns>
        List<IPerson> GetRecoveredPeople { get; }
    }
}
