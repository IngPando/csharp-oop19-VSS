﻿using System;


namespace csharp_oop19_VSS.model.virus
{
    public interface IVirusFactory
    {
        IVirus CreateVirus();
    }
}
